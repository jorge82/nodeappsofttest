const Trades = require("../models/trades");
const { Sequelize, DataTypes } = require("sequelize");
const Op = Sequelize.Op;

const MIN_SHARES = 1;
const MAX_SHARES = 100;

const controller = {};
/* Creation of a stock
returns: status 400 is error , 201 and the created stock in json otherwhise  */
controller.create = async (req, res) => {
  try {
    if (req.body.type != "buy" && req.body.type != "sell") {
      return res.status(400).end();
    }
    if (req.body.shares < MIN_SHARES || req.body.shares > MAX_SHARES) {
      return res.status(400).end();
    }
    const response = await Trades.create({
      type: req.body.type,
      user_id: req.body.user_id,
      symbol: req.body.symbol,
      shares: req.body.shares,
      price: req.body.price,
      timestamp: req.body.timestamp
    })
      .then(function (data) {
        return res.status(201).json(data);
      })
      .catch((error) => {
        return res.status(400);
      });
  } catch (e) {
    console.log(e);
  }
};
/* Find all stocks with optional query parameters: type and user
returns: status 200 and an a json array  */
controller.findAll = async (req, res) => {
  try {
    const type = req.query.type;

    if (type) {
      if (type != "buy" && type != "sell") {
        return res.status(200).json([]);
      }
    }

    let whereCondition = {};
    if (req.query.user_id) {
      whereCondition["user_id"] = parseInt(req.query.user_id);
    }
    if (req.query.type) {
      whereCondition["type"] = req.query.type;
    }

    const response = await Trades.findAll({
      where: { [Op.and]: whereCondition },
      raw: true
    })
      .then((data) => {
        return res.status(200).json(data);
      })
      .catch((e) => {
        return res.status(200);
      });
  } catch (e) {
    console.log(e);
  }
};
/* Find all the stock with id passed as parameter
returns: status 200 and an a json with the stock if found else returns status 404 and the message "ID not found"  */

controller.findOne = async (req, res) => {
  try {
    const searched_id = req.params.id;

    const response = await Trades.findOne({
      where: { id: parseInt(searched_id) },
      raw: true
    })
      .then((data) => {
        if (!data) {
          return res.status(404).send("ID not found");
        }
        return res.status(200).json(data);
      })
      .catch((e) => {
        return res.status(200);
      });
  } catch (e) {
    console.log(e);
  }
};
module.exports = controller;
