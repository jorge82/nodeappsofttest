// Uncomment the code below to use Sequelize ORM
const { Sequelize, DataTypes } = require("sequelize");
const sequelize = new Sequelize("sqlite::memory:");

// Uncomment the code below to use Mongoose ORM
// const mongoose = require('mongoose');

// Insert your model definition below

const Trades = sequelize.define(
  "trades",
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    type: {
      type: DataTypes.STRING,
      validate: {
        isIn: [["buy", "sell"]]
      }
    },
    user_id: DataTypes.INTEGER,
    symbol: DataTypes.STRING,
    shares: {
      type: DataTypes.INTEGER,
      validate: {
        max: 100,
        min: 1
      }
    },

    price: DataTypes.INTEGER,
    timestamp: DataTypes.INTEGER
  },

  { timestamps: false }
);
module.exports = Trades;
