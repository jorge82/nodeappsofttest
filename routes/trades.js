const express = require("express");
const router = express.Router();
const controller = require("../controllers/trades");

router.post("/", controller.create);
router.get("/", controller.findAll);
router.get("/:id", controller.findOne);
router.patch("/:id", (req, res) => {
  res.status(405).end();
});
router.delete("/:id", (req, res) => {
  res.status(405).end();
});

router.put("/:id", (req, res) => {
  res.status(405).end();
});

module.exports = router;
